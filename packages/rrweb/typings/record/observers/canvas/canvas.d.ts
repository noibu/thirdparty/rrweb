import { blockClass, IWindow, listenerHandler } from '../../../types';
export default function initCanvasContextObserver(win: IWindow, blockClass: blockClass, blockSelector: string | null): listenerHandler;
